import pickle
import numpy as np


def get_similarity_multi(v12_list) -> float:
    v1 = v12_list[0]
    v2 = v12_list[1]
    dot = np.dot(v1, v2)
    norm1 = np.linalg.norm(v1)
    norm2 = np.linalg.norm(v2)
    cos = dot / (norm1 * norm2)
    return cos


def get_similarity_legacy(v1, v2) -> float:
    dot = np.dot(v1, v2)
    norm1 = np.linalg.norm(v1)
    norm2 = np.linalg.norm(v2)
    cos = dot / (norm1 * norm2)
    return cos


def find_best_song_legacy(rv) -> int:
    sim_list = []
    for l in lyric_vectors:
        lv = l[1]
        sim = get_similarity_legacy(rv, lv)
        sim_list.append(sim)
    index0 = sim_list.index(max(sim_list))
    sim_list.pop(index0)
    index1 = sim_list.index(max(sim_list))
    sim_list.pop(index1)
    index2 = sim_list.index(max(sim_list))
    sim_list.pop(index2)
    return index0, index1, index2


if __name__ == '__main__':
    fi_l = open('vector_lyric_01bigger', 'rb')
    fi_r = open('spot_vector_avr01bigger', 'rb')
    # fo = open('spot-lyric_matching', 'wb')
    lyric_vectors = pickle.load(fi_l)
    review_vectors_avr = pickle.load(fi_r)
    match = []
    id_freq0 = {}
    id_freq1 = {}
    id_freq2 = {}
    for spot_item in review_vectors_avr:
        spot_name = spot_item[0]
        spot_vector = spot_item[1]
        best_id0, best_id1, best_id2 = find_best_song_legacy(spot_vector)
        print(spot_name, best_id0, best_id1, best_id2)
        if best_id0 in id_freq0:
            id_freq0[best_id0] += 1
        else:
            id_freq0[best_id0] = 1
        if best_id1 in id_freq1:
            id_freq1[best_id1] += 1
        else:
            id_freq1[best_id1] = 1
        if best_id2 in id_freq2:
            id_freq2[best_id2] += 1
        else:
            id_freq2[best_id2] = 1
        match.append([spot_name, best_id0, best_id1, best_id2])
    print()
    for k, v in sorted(id_freq0.items(), key=lambda p: p[1], reverse=True):
        print(k, v)
    print()
    for k, v in sorted(id_freq1.items(), key=lambda p: p[1], reverse=True):
        print(k, v)
    print()
    for k, v in sorted(id_freq2.items(), key=lambda p: p[1], reverse=True):
        print(k, v)
    fi_l.close()
    fi_r.close()
