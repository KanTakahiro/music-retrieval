import csv
import sqlite3
import re
import nltk
from nltk.stem import WordNetLemmatizer
import mojimoji
from tqdm import tqdm
import langdetect


def sentence_lemmatizer(text: str):
    # this function filter only noun, verb, adj and adv, then convert them to original format
    POS_list = ['FW', 'JJ', 'JJR', 'JJS', 'NN', 'NNS', 'NNP', 'NNPS',
                'RB', 'RBR', 'RBS', 'VB', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ']
    sentence_tags = nltk.pos_tag(nltk.word_tokenize(text))
    output = ''
    for pos_tag in sentence_tags:
        if pos_tag[1] in POS_list:
            output += pos_tag[0] + ' '
    return output


fi = open('tripadvisor_en_review.tsv', 'r')
reader = csv.reader(fi, delimiter='\t')
wordnet_lemmatizer = WordNetLemmatizer()
db = 'db.sqlite'
conn = sqlite3.connect(db)
# conn.isolation_level = None
cur = conn.cursor()
cur.execute("DROP TABLE IF EXISTS reviews")
cur.execute("CREATE TABLE IF NOT EXISTS reviews (id INTEGER PRIMARY KEY, hotel TEXT, review TEXT)")
counter = 0
for row in tqdm(reader, total=101857):
    # print(counter)
    hotel = row[0]
    # ranking = int(row[1])
    review = row[2]
    review_f2h = mojimoji.zen_to_han(review)
    if langdetect.detect(review_f2h) == 'en':
        review_re = re.sub('\[.*?\]|\.|\,|\(|\)|\?|!|"|\d+|-|\+', ' ', review_f2h).lower()
        review_re_le = sentence_lemmatizer(review_re)
        if len(review_re_le) > 10:
            cur.execute("INSERT INTO reviews VALUES (?, ?, ?)", (counter, hotel, review_re_le))
            counter += 1
    else:
        print(review)

conn.commit()
conn.close()
