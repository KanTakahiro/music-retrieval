import gensim
import logging
from tqdm import tqdm

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
fi = open('lyrics_for_training', 'r')
lines = fi.readlines()
data = []
# counter = 0
for line in tqdm(lines):
    # print(counter)
    data.append(gensim.utils.simple_preprocess(line))
    # counter += 1
model = gensim.models.Word2Vec(
    data, size=300, window=10, min_count=2, workers=8, iter=10)
model.save('lyrics.model')
