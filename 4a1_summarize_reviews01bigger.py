from sklearn.feature_extraction.text import TfidfVectorizer
from tqdm import tqdm
import sqlite3

db = 'db.sqlite'
conn = sqlite3.connect(db)
# conn.isolation_level = None
cur = conn.cursor()
cur.execute('SELECT COUNT(*) FROM reviews')
row_num = int(cur.fetchall()[0][0])
docs = []
for i in range(row_num):
    cur.execute('SELECT review FROM reviews WHERE id={}'.format(i))
    r = cur.fetchall()[0][0]
    docs.append(r)
cur.execute("DROP TABLE IF EXISTS reviews01bigger")
cur.execute("CREATE TABLE IF NOT EXISTS reviews01bigger (id INTEGER PRIMARY KEY, hotel TEXT, review TEXT)")
vectorizer = TfidfVectorizer()
X = vectorizer.fit_transform(docs)
words = vectorizer.get_feature_names()
tfidf_vec = X.toarray()
tfidf_min = 0.1
for doc_id, vec in tqdm(zip(range(len(docs)), tfidf_vec), total=len(docs)):
    r_summarized = ''
    for w_id, tfidf in sorted(enumerate(vec), key=lambda x: x[1], reverse=True):
        if tfidf > tfidf_min:
            w = words[w_id]
            r_summarized += w + ' '
        if tfidf == 0.0:
            break
    cur.execute('SELECT hotel FROM reviews WHERE id={}'.format(doc_id))
    hotel = cur.fetchall()[0][0]
    cur.execute("INSERT INTO reviews01bigger VALUES (?, ?, ?)", (doc_id, hotel, r_summarized))

conn.commit()
conn.close()
