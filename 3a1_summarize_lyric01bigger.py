from sklearn.feature_extraction.text import TfidfVectorizer
from tqdm import tqdm
import sqlite3

db = 'db.sqlite'
conn = sqlite3.connect(db)
# conn.isolation_level = None
cur = conn.cursor()
cur.execute('SELECT COUNT(*) FROM songs')
row_num = int(cur.fetchall()[0][0])
docs = []
for i in range(row_num):
    cur.execute('SELECT lyrics FROM songs WHERE id={}'.format(i))
    r = cur.fetchall()[0][0]
    docs.append(r)
cur.execute("DROP TABLE IF EXISTS songs01bigger")
cur.execute("CREATE TABLE IF NOT EXISTS songs01bigger (id INTEGER PRIMARY KEY, title TEXT, artist TEXT, lyrics TEXT)")
vectorizer = TfidfVectorizer()
X = vectorizer.fit_transform(docs)
words = vectorizer.get_feature_names()
tfidf_vec = X.toarray()
tfidf_min = 0.1
for doc_id, vec in tqdm(zip(range(len(docs)), tfidf_vec), total=len(docs)):
    l_summarized = ''
    for w_id, tfidf in sorted(enumerate(vec), key=lambda x: x[1], reverse=True):
        if tfidf > tfidf_min:
            w = words[w_id]
            l_summarized += w + ' '
        if tfidf == 0.0:
            break
    cur.execute('SELECT title FROM songs WHERE id={}'.format(doc_id))
    title = cur.fetchall()[0][0]
    cur.execute('SELECT artist FROM songs WHERE id={}'.format(doc_id))
    artist = cur.fetchall()[0][0]
    cur.execute("INSERT INTO songs01bigger VALUES (?, ?, ?, ?)", (doc_id, title, artist, l_summarized))

conn.commit()
conn.close()
