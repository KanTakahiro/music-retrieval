import json
import langdetect
import sqlite3
from tqdm import tqdm
import re
import nltk


def sentence_lemmatizer(text: str):
    # this function filter only noun, verb, adj and adv, then convert them to original format
    POS_list = ['FW', 'JJ', 'JJR', 'JJS', 'NN', 'NNS', 'NNP', 'NNPS',
                'RB', 'RBR', 'RBS', 'VB', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ']
    sentence_tags = nltk.pos_tag(nltk.word_tokenize(text))
    output = ''
    for pos_tag in sentence_tags:
        if pos_tag[1] in POS_list:
            output += pos_tag[0] + ' '
    return output


db = 'db.sqlite'
connection = sqlite3.connect(db)
# connection.isolation_level = None
cursor = connection.cursor()
fi = open('azlyrics_utf8', 'r')
fo = open('lyrics_for_training.txt', 'w')
lines = fi.readlines()
counter = 0
cursor.execute("DROP TABLE IF EXISTS songs")
cursor.execute(
    "CREATE TABLE IF NOT EXISTS songs (id INTEGER PRIMARY KEY, title TEXT, artist TEXT, lyrics TEXT)")
for line in tqdm(lines):
    # print(counter)
    try:
        if len(line) > 1:
            data = json.loads(line.split('\t')[1])
            artist = data['artist']
            url = data['url']
            title = data['title']
            lyric = data['lyrics'].replace('\n', ' ')
            lyric_re = re.sub(
                '\[.*?\]|\.|\,|\(|\)|\?|"|\d+', ' ', lyric).lower()
            lyric_re_le = sentence_lemmatizer(lyric_re)
            '''
            album_track = data['albums']['track']
            album_title = data['albums']['title']
            album_year = data['albums']['year']
            '''
            if langdetect.detect(lyric) == 'en' and len(lyric_re_le) > 10:
                cursor.execute("INSERT INTO songs VALUES (?, ?, ?, ?)",
                               (counter, title, artist, lyric_re_le))
                counter += 1
                fo.write(lyric_re_le + '\n')
            # else:
                # print(lyrics)
    except langdetect.lang_detect_exception.LangDetectException:
        print(lyric)
connection.commit()
connection.close()
fi.close()
fo.close()
