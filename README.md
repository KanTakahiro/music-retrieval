# Music Retrieval Focusing on Lyrics with Summary of Tourist-spot Reviews Based on Shared Word-vectors

A system for recommending lyrics similar to the context of the reviews of a tourist-spot. The method is based on the technique of distributed representation of words.

## Workflow Description

### Source Data

The source files `azlyrics_utf8.txt` and `tripadvisor_en_review.tsv` required as the system input are compressed into `source_data.zip`.

1. ``azlyrics_utf8.txt`` lyrics from [AZLyrics](https://www.azlyrics.com/ )
2. ``tripadvisor_en_review.tsv`` tourist spot reviews from [Tripadvisor](https://www.tripadvisor.com/ )

**source_data.zip was deleted for a copyright concern**

### Program Files Description

**Tips:**

- The naming format of program files is like `NUMBER + _ + DESCRIPTION`. Number shows the order in the workflow, decimal point means this script is a sub processing of another script. Point alphabet 'a' means this file is an alternative. For example `4a1_summarize_reviews01bigger.py` is a variant version of `4_review_into_db`. Description shows the purpose of the script.

|File Name|Description|Input|Output|
|---------|-----------|-----|------|
|1_lyric_into_sqlite.py|extract English lyric information and store into a SQLite database as table "songs"|azlyrics_utf8.txt|table "songs" in db.sqlite && lyrics_for_training.txt|
|2_word2vec.py|train model with word2vec|lyrics_for_training.txt|lyrics.model|
|3_lyric_vector.py|calculate the average vector for each lyric and build a id-vector list then dump the list to a file|table "songs" in db.sqlite && lyrics.model|vector_lyric|
|3a1_summarize_lyric01bigger.py|summarize lyrics with tfidf, the words with a tfidf value > 0.1 will be kept|table "songs" in db.sqlite|table "songs01bigger" in db.sqlite|
|4_review_into_db.py|extract and lemmatize review texts and store them into a table "reviews" in db.sqlite|tripadvisor_en_review.tsv|table "reviews" in db.sqlite|
|4a1_summarize_reviews01bigger.py|summarize reviews with tfidf, the words with a tfidf value > 0.1 will be kept|tripadvisor_en_review.tsv|table "reviews01bigger" in db.sqlite|
|5_spot_vector.py|merge all review contents of each tourist spot and calculate the vector of the merged text as the vector of spot|table "reviews"(or "reviews01bigger") in db.sqlite && lyrics.model|spot_vector_avr or spot_vector_avr01bigger|
|6_match_spot_and_lyric.py|calculate the similarity of lyric vector and sopt vector, print the song title with highest similarity with spot |vector_lyric_01bigger && spot_vector_avr01bigger|None(show match result in console)|
