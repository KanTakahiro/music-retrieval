'''
This script merge all review contents of each spot and calculate the vector of the merged text as the vector of spot.
'''

from gensim.models import Word2Vec
import sqlite3
import pickle


def get_avr_vector(text: str):
    # text = re.sub('\[.*?\]', ' ', text).replace(',', ' ').replace('.', ' ')
    words = text.split()
    sum = 0
    counter = 0
    for w in words:
        if len(w) > 0:
            try:
                v = model.wv[w]
            except KeyError:
                # print('KeyError:', w)
                continue
            sum += v
            counter += 1
    avr_v = sum / counter
    return avr_v


model = Word2Vec.load("lyrics.model")
db = 'db.sqlite'
fo = open('spot_vector_avr01bigger', 'wb')
conn = sqlite3.connect(db)
# conn.isolation_level = None
cur = conn.cursor()
cur.execute('SELECT COUNT(*) FROM reviews01bigger')
row_num = int(cur.fetchall()[0][0])

spot_name_dict = {}
for i in range(row_num):
    cur.execute('SELECT hotel FROM reviews01bigger WHERE id={}'.format(i))
    spot_name = cur.fetchall()[0][0]
    if spot_name not in spot_name_dict:
        spot_name_dict[spot_name] = [i, i]
    else:
        spot_name_dict[spot_name][1] = i
print('spot_name_dict completed')

spot_review_all_list = []
for spot in spot_name_dict:
    start = spot_name_dict[spot][0]
    end = spot_name_dict[spot][1]
    review_all = ''
    print(spot)
    for i in range(start, end+1):
        cur.execute('SELECT review FROM reviews01bigger WHERE id={}'.format(i))
        review = cur.fetchall()[0][0]
        review_all += review + ' '
    spot_review_all_list.append([spot, review_all])
print('spot_review_all_list completed')

spot_vector_avr = []
for item in spot_review_all_list:
    spot_name = item[0]
    review_all = item[1]
    spot_vector = get_avr_vector(review_all)
    spot_vector_avr.append([spot_name, spot_vector])
print('spot_vector_avr completed')

pickle.dump(spot_vector_avr, fo)
conn.commit()
conn.close()
print('done')
