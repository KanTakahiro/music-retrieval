from gensim.models import Word2Vec
import sqlite3
import pickle
from tqdm import tqdm


def get_avr_vector(text: str, id: int):
    # text = re.sub('\[.*?\]', ' ', text).replace(',', ' ').replace('.', ' ')
    words = text.split()
    sum = 0
    counter = 0
    for w in words:
        if len(w) > 0:
            try:
                v = model.wv[w]
            except KeyError:
                # print('KeyError:', w)
                continue
            sum += v
            counter += 1
    try:
        avr_v = sum / counter
        return avr_v
    except ZeroDivisionError:
        print(text, id)
        input()
        return None


model = Word2Vec.load("lyrics.model")
db = 'db.sqlite'
conn = sqlite3.connect(db)
# conn.isolation_level = None
cur = conn.cursor()
vec_list = []
fo = open('vector_lyric_01bigger', 'wb')
cur.execute('SELECT COUNT(*) FROM songs01bigger')
row_num = int(cur.fetchall()[0][0])
for i in tqdm(range(row_num)):
    # print(i)
    cur.execute('SELECT lyrics FROM songs01bigger WHERE id={}'.format(i))
    lyric_text = cur.fetchall()[0][0]
    average_vector = get_avr_vector(lyric_text, i)
    if average_vector is not None:
        vec_list.append([i, average_vector])
pickle.dump(vec_list, fo)
fo.close()
conn.close()
